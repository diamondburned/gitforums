/* 
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

//** Functions
function tokenURI() {
	var url = new URL(window.location.href.replace('#', '?'))

	let access_token = url.searchParams.get("access_token")
	let token_type   = url.searchParams.get("token_type")

	if (access_token && token_type) {
		var time = new Date();
		var now = time.getTime();
		var expireTime = now + 86400 * 1000;
		time.setTime(expireTime)
		var expires = time.toUTCString()

		console.log(`access_token=${access_token}; expires=${expires}; path=/`)
		document.cookie = `access_token=${access_token}; expires=${expires}; path=/`
	} else {
		return
	}
}

function loadAuth(apikey, uri) {
	let cookie = document.cookie.split('; ')
	cookie.forEach(each => {
		if (each.startsWith("access_token")) {
			window.access_token = each.split("=")[1]

            loadProfile(access_token)
		} else {
			let loginLink = document.createElement("a")
			loginLink.setAttribute("class", "gitforums authentication login")
			loginLink.setAttribute("href", "#")
			loginLink.setAttribute("onclick", `authenticate("${apikey}", "${uri}")`)
			loginLink.innerHTML = "Login using GitLab"

            document.querySelector('div.gitforums.login.main').appendChild(loginLink)
		}
    })	
    
    return window.access_token
}

function authenticate(apikey, uri) {
	let endpoint  = "https://gitlab.com/oauth/authorize"
		endpoint += `?client_id=${encodeURIComponent(apikey)}`
		endpoint += `&redirect_uri=${encodeURIComponent(uri)}`
		endpoint += `&response_type=token`

	window.location.replace(endpoint)
}

function loadProfile(apikey) {
	var request = new XMLHttpRequest()

	request.onreadystatechange = (function() {
		if (this.readyState == 4) {
			if (this.status == 200) {
				appendToHTML(this.responseText)
			} else {
				let error
				error += `Error!\n`
				error += this.responseText
				
				console.error(error)
				return
			}
		}
	})

	request.open("GET", "https://gitlab.com/api/v4/user")
	request.setRequestHeader('Authorization', `Bearer ${apikey}`)

	request.send()

	function appendToHTML(rawJSON) {
		var user = JSON.parse(rawJSON)

		let avatar = document.querySelector("div.gitforums.login.avatar")
		avatar.setAttribute("class", "gitforums login avatar")
		avatar.setAttribute("style", `background-image: url("${user.avatar_url}")`)

		let name = document.querySelector("div.gitforums.login.main")
		name.setAttribute("class", "gitforums login name")

		let link = document.createElement("a")
		link.setAttribute("class", "gitforums login name")
		link.setAttribute("href", user.web_url)

		if (user.name == user.username) {
			let Name = user.name
			link.innerHTML = Name
		} else {
			let Name = `${user.name} (${user.username})`
			link.innerHTML = Name
		}

		name.appendChild(link)
	}
}

function load(projectName, uri, apikey) {
	var url = new URL(window.location.href)

	if (url.searchParams.get("id")) {
        if (localStorage.getItem("Issues")) {
            loadPost(projectName, url.searchParams.get("id"), apikey)
        } else {
            loadIssues(projectName, uri, apikey, "NOPRNT")
        }
	} else {
		loadIssues(projectName, uri, apikey, "")
	}
}

function prntErr(content) {
    let errorDiv = document.createElement("div")
        errorDiv.setAttribute("class", "gitforums body error")
        errorDiv.innerHTML = `${content}`
        
    document.querySelector('div.gitforums.body').appendChild(errorDiv)
}

function loadPost(projectName, id, apikey) {
	var request = new XMLHttpRequest()
	request.onreadystatechange = (function() {
		if (this.readyState == 4) {
			if (this.status == 200) {
				printPost(this.responseText, id)
            } else if (this.status == 429) {
                prntErr("You're being rate throttled. Please slow down!")
            } else {
				let error
				error += `Error!\n`
				error += this.responseText

				console.error(error)
				return
			}
		}
	})

	let escapedName = encodeURIComponent(projectName)
    request.open("GET", `https://gitlab.com/api/v4/projects/${escapedName}/issues/${id}/notes?sort=asc&order_by=created_at`)

    if (!apikey) {
        prntErr("You need to login to view issues (blame GitLab)")
        return
    } else {
        request.setRequestHeader('Authorization', `Bearer ${apikey}`)
    }

	request.send()

	function printPost(json, id) {
        console.log(id)
        let postsJSON = JSON.parse(json)
        
        let Issues = JSON.parse(localStorage.getItem("Issues"))

        Object.keys(Issues).forEach((key) => {
            if (Issues[key].iid == id) {
                window.issueID_key = key
                return
            }
        })

        let issue  = Issues[window.issueID_key]

        document.querySelector('div.gitforums.body').innerHTML = ""

		let table = document.createElement("table")
            table.setAttribute("class", "gitforums comment table")
        
        let row = document.createElement("tr")
            row.setAttribute("class", "gitforums comment row master")

        let cell = document.createElement("td")
            cell.setAttribute("class", "gitforums comment cell master")
            
        let avatarDiv = document.createElement("div")
            avatarDiv.setAttribute("class", "gitforums comment avatar master")
            avatarDiv.setAttribute("style", `background-image: url("${issue.author.avatar_url}")`)

        let infoDiv = document.createElement("div")
            infoDiv.setAttribute("class", "gitforums comment info master")
        
        let title = document.createElement("p")
            title.setAttribute("class", "gitforums comment info title master")
            title.innerHTML = marked(issue.title)

        let body = document.createElement("p")
            body.setAttribute("class", "gitforums comment info body master")
            body.innerHTML = marked(issue.description)

        infoDiv.appendChild(title)
        infoDiv.appendChild(body)

        cell.appendChild(avatarDiv)
        cell.appendChild(infoDiv)

        row.appendChild(cell)

        document.querySelector('div.gitforums.body').appendChild(row)

		Object.keys(postsJSON).forEach((key) => {
            if (postsJSON[key].system)
                return

			let row = document.createElement("tr")
			    row.setAttribute("class", "gitforums comment row")

			let cell = document.createElement("td")
			    cell.setAttribute("class", "gitforums comment cell")

			let infoDiv = document.createElement("div")
			    infoDiv.setAttribute("class", "gitforums comment infodiv")
			let subinfoDiv = document.createElement("div")
			    subinfoDiv.setAttribute("class", "gitforums comment subinfodiv")

			let avatarLink = document.createElement("a")
			    avatarLink.setAttribute("class", "gitforums comment avatar")
			    avatarLink.setAttribute("href", postsJSON[key].author.web_url)
			let avatar = document.createElement("img")
			    avatar.setAttribute("class", "gitforums comment avatar")
			    avatar.setAttribute("src", postsJSON[key].author.avatar_url)
			avatarLink.appendChild(avatar)
			
			let body = document.createElement("p")
			    body.setAttribute("class", "gitforums comment body")
			    body.innerHTML = marked(postsJSON[key].body)

			infoDiv.appendChild(avatarLink)
			infoDiv.appendChild(body)

			let author = document.createElement("a")
                author.setAttribute("class", "gitforums comment author")
                author.setAttribute("href", postsJSON[key].author.web_url)
            function last(created, updated) {
                if (created == updated) {
                    return `created at ${timesince(created)}`
                } else {
                    return `created at ${timesince(created)}, last updated ${updated}`
                }
            }
			if (postsJSON[key].author.name == postsJSON[key].author.username) {
				let authorName = postsJSON[key].author.name
				    author.innerHTML = `${authorName}, ${last(postsJSON[key].created_at, postsJSON[key].updated_at)}`
			} else {
				let authorName = `${postsJSON[key].author.name} (${postsJSON[key].author.username})`
				    author.innerHTML = `${authorName}, ${last(postsJSON[key].created_at, postsJSON[key].updated_at)}`
			}

			subinfoDiv.appendChild(author)

			cell.appendChild(infoDiv)
			cell.appendChild(subinfoDiv)

			row.appendChild(cell)

			document.querySelector('div.gitforums.body').appendChild(row)
		})
	}
}

function loadIssues(projectName, uri, apikey, options) {
    var IssuesLS = localStorage.getItem("Issues")
	var timestamp = localStorage.getItem("Timestamp")
    if ((!options && !IssuesLS) || options.indexOf("RECACH") !== -1) {
	    var request = new XMLHttpRequest()
	    request.onreadystatechange = (function() {
	    	if (this.readyState == 4) {
	    		if (this.status == 200) {
					let timestamp = new Date()
                    localStorage.setItem("Issues", this.responseText)
					localStorage.setItem("Timestamp", timestamp)
                    if (options.indexOf("NOPRNT") == -1) {
                        ListIssues(this.responseText, uri)
                    }
                } else {
	    			let error
	    			error += `Error!\n`
	    			error += this.responseText

	    			console.error(error)
	    			return
	    		}
	    	}
	    })

	    let escapedName = encodeURIComponent(projectName)
	    request.open("GET", `https://gitlab.com/api/v4/projects/${escapedName}/issues?order_by=updated_at&sort=desc`)

        request.send()
    } else {
        ListIssues(IssuesLS, timestamp, uri)
    }

	function ListIssues(issue, timestamp, location) {
		let issuesJSON = JSON.parse(issue)

		let table = document.createElement("table")
		table.setAttribute("class", "gitforums list table")
        
        let row = document.createElement("tr")
			row.setAttribute("class", "gitforums list row refresh")

		let cell = document.createElement("td")
            cell.setAttribute("class", "gitforums list cell refresh")
            
        let refresh = document.createElement("a")
            refresh.setAttribute("class", "gitforums list refresh")
            refresh.setAttribute("href", '#')
            refresh.setAttribute("onclick", `loadIssues("${projectName}", "${uri}", "${apikey}", "RECACH")`)
			refresh.onclick = function() {
				document.querySelector('a.gitforums.list.refresh').innerHTML = `Refreshing...`
			}
			refresh.innerHTML = `Refresh / ${timesince(timestamp)}`

        cell.appendChild(refresh)

        row.appendChild(cell)

		document.querySelector('div.gitforums.body').innerHTML = ''
        document.querySelector('div.gitforums.body').appendChild(row)

		issuesJSON.forEach((issue) => {
			let row = document.createElement("tr")
			    row.setAttribute("class", "gitforums list row")

			let cell = document.createElement("td")
			    cell.setAttribute("class", "gitforums list cell")

			// Make a bunch of divs
			let avatarDiv = document.createElement("div")
			let infoDiv = document.createElement("div")
			let subinfoDiv = document.createElement("div")

			avatarDiv.setAttribute("class", "gitforums list avatar")
			avatarDiv.setAttribute("style", `background-image: url("${issue.author.avatar_url}")`)

			infoDiv.setAttribute("class", "gitforums list infodiv")
			subinfoDiv.setAttribute("class", "gitforums list subinfodiv")

			let title = document.createElement("a")
			    title.setAttribute("class", "gitforums list title")
			    title.innerHTML = issue.title
			    title.setAttribute("href", `?id=${issue.iid}`)

			let author = document.createElement("a")
                author.setAttribute("class", "gitforums list author")
                author.setAttribute("href", issue.author.web_url)
			if (issue.author.name == issue.author.username) {
				let authorName = issue.author.name
				author.innerHTML = `${authorName}, ${timesince(issue.updated_at)}`
			} else {
				let authorName = `${issue.author.name} (${issue.author.username})`
				author.innerHTML = `${authorName}, ${timesince(issue.updated_at)}`
			}

			let stats = document.createElement("div")
			    stats.setAttribute("class", "gitforums list stats")
			    stats.innerHTML += `<p class="gitforums list stats upvotes">${issue.upvotes} 👍 </p>`
			    stats.innerHTML += `<p class="gitforums list stats downvotes">${issue.downvotes} 👎 </p>`
			    stats.innerHTML += `<p class="gitforums list stats comments">${issue.user_notes_count} 🗨</p>`

			subinfoDiv.appendChild(author)
			subinfoDiv.appendChild(stats)

			infoDiv.appendChild(title)
			infoDiv.appendChild(subinfoDiv)

			cell.appendChild(avatarDiv)
			cell.appendChild(infoDiv)

			row.appendChild(cell)

			document.querySelector('div.gitforums.body').appendChild(row)
		})
	}
}

//** Stolen functions
// https://stackoverflow.com/a/23352499
function timesince(timeStamp) {
	let now = new Date()
	let that = new Date(timeStamp)
	let secondsPast = (now.getTime() - that.getTime()) / 1000

	if(secondsPast < 60){
		return Math.floor(secondsPast) + ' seconds ago'
	} else if(secondsPast < 3600){
		return Math.floor(secondsPast/60) + ' minutes ago'
	} else if(secondsPast <= 86400){
		return Math.floor(secondsPast/3600) + ' hours ago'
	} else if(secondsPast > 86400){
		let day = that.getDate();
		let month = that.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ","");
		let year = that.getFullYear() == now.getFullYear() ? "" :  " "+that.getFullYear();
		return day + " " + month + year;
	}
  }